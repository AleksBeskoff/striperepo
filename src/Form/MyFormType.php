<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MyFormType extends AbstractType 
{
    public function buildForm(FormBuilderInterface $builder, array $options ) {
        //parent::buildForm( $builder, $options );
        //dump($options); 
        $builder
            ->add('name')            
            ->add('email')
            ->add('phoneNumber')
            ->add('token')
            ->add('amount')
            ->add('currency')
            ->add('description')
            ->add('save', SubmitType::class, array('label' => 'Отправить'));
    }
}
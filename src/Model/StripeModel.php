<?php

namespace App\Model;

use App\Controller\StripeController;
use App\Repository\StripeRepo\StripeBase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StripeModel 
{
	private $doctrine;
	private $validator;
	
	/**
	 * объект класса работы с платежным сервисом
	 *
	 * @var stripeController
	 */
	private $stripeController;	

	public function __construct(object $doctrine, ValidatorInterface $validator, StripeController $stripeController)
	{
		$this->doctrine = $doctrine;
		$this->validator = $validator;
		$this->stripeController = $stripeController;		
	}


	public function setDataForm(object $operationDetails):array	
	{		
		$stripeBase = new StripeBase($this->doctrine,$this->validator,$operationDetails);
		$arrRet = $stripeBase->JobBase();
		if(count($arrRet)>0){
			return $arrRet;
		}
		$arrRet = $stripeBase->EnterStripe($this->stripeController);
		$arrRetTmp = $stripeBase->EnterBase($arrRet['status']);
		if(count($arrRetTmp) > 0){
			$arrRet = $arrRetTmp;
		}				
		return $arrRet;
	}
}
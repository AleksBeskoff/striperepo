<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Component\HttpFoundation\Response;

// ПОЧЕМУ StripeController есть контроллер, когда это клиетский модуль? Это StripeClient, скорее, в пакете stripe. Логика здесь перемешана - класс как общается со страйпом + отдает шаблоны
class StripeController extends AbstractController 
{
    /**
     * запрос транзакции на платежном сервисе
     * возвращаем данные по операции, либо содержание ошибки на сервисе
     * @return array
     */
    public function showData3(string $token, float $amount, string $currency, string $desctiption):array 
    {
        try{
            $stripe = new \Stripe\StripeClient(
                'sk_test_mxC5fKbG0huSlzC0sEwqkaQG00f7g7sbOs'
            );        
            $dumper = $stripe->charges->create([
                'amount' => $amount,
                'currency' => $currency,
                'source' => $token,
                'description' => $desctiption,
            ]);
            if($dumper->status!=='succeeded'){
                throw new Exception('Paiment False',402);
            }                   
        }catch(Exception $e){            
            return array('status'=> false,
                'errorMessage' => $e->getMessage(),
                'errorCode' => $e->getCode() 
            );                
        }
        return array('status'=> true,
            'id' => $dumper->id            
        ); 
    }    
    
    public function showData() 
    {
        try{
            $stripe = new \Stripe\StripeClient(
                'sk_test_mxC5fKbG0huSlzC0sEwqkaQG00f7g7sbOs'
            );        
            $dumper = $stripe->charges->create([
                'amount' => 20000000,
                'currency' => 'usd',
                'source' => 'tok_amex',
                'description' => 'My First Test Charge (created for API docs)',
            ]);
            dump($dumper->status);
            if($dumper->status!=='succeeded'){
                throw new Exception('Paiment False',402);
            }                   
        }catch(Exception $e){            
            return $this->render('stripe.html.twig', [
                'client_secret'=>$e->getMessage().', '.$e->getCode(),
            ]);
        }
        $client_secret = json_encode(array('client_secret' => $dumper->status));
        return $this->render('stripe.html.twig', [
            'client_secret'=>$client_secret,
        ]);
    }

    public function getSeansKey():string 
    {
        \Stripe\Stripe::setApiKey('sk_test_mxC5fKbG0huSlzC0sEwqkaQG00f7g7sbOs');
        $intent = \Stripe\PaymentIntent::create([
            'amount' => 1099,
            'currency' => 'usd',
        ]);
        
        $client_secret = json_encode(array('client_secret' => $intent->client_secret));

        return $client_secret;
    }    
}
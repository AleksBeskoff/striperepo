<?php
namespace App\Controller;

use App\Controller\Model\OperationDetails;
use App\Model\StripeModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Controller\StripeController;
use App\Form\MyFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DbController extends AbstractController
{
	private $validator;
    
    public function __construct(ValidatorInterface $validator)
	{
        $this->validator = $validator;        
    }

    public function AddDataBase(Request $request,StripeController $stripeController):object
    {
        $client_secret = $stripeController->getSeansKey();
        $stripemodel = new StripeModel($this->getDoctrine(), $this->validator, $stripeController);
        
        $data = (object)array(
            'name'=>'Aleks Beskoff',
            'email'=>'aleks.beskoff@mail.ru',
            'phoneNumber'=>'+375296681201',
            'token'=>'tok_chargeDeclinedInsufficientFunds',
            'amount'=>'2000',
            'currency'=>'USD',
            'description'=>'My First Transaction for payment'
        );

        $form = $this->createForm(MyFormType::class, $data);
        
        $form->handleRequest($request);        

        if ($form->isSubmitted()) {
            $arrData = $request->request->all()['my_form'];
            $operationDetails = new OperationDetails(
                $arrData['name'],
                $arrData['email'],
                $arrData['phoneNumber'],
                $arrData['token'],
                $arrData['amount'],
                $arrData['currency'],
                $arrData['description']
            );           
            //dump($stripemodel->setDataForm($operationDetails));
            return $this->redirectToRoute('task_success',$stripemodel->setDataForm($operationDetails)); 
        }
        
        return $this->render('form.html.twig',[
            'form' => $form->createView(),            
            'flag' => 'flag',
        ]);

    }

     /**
     * Undocumented function
     *
     * @return object
     */
    public function AddDataBase2(Request $request):object 
    {
        $stripe_code = new StripeController;
        $client_secret = $stripe_code->getSeansKey();
        $flag = true;
        
        //$data = new OperationDetails('aleks.beskogff--mail.ru', $client_secret);
        $data = (object)array('email'=>'aleks.beskoff@mail.ru','KodOperation'=>$client_secret);

        $form = $this->createForm(MyFormType::class, $data);
        
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $arrData = $request->request->all()['my_form'];
            $data = (object)array('email'=>$arrData['email'],'KodOperation' => $arrData['KodOperation']);            
        }
        
        return $this->render('form.html.twig',[
            'form' => $form->createView(),            
            'flag' => $flag ? 1 : 0,
        ]);        
    }  

    /**
     * Выводим результаты выполения операции с генерецией ответа пользователю в виде html-кода
     *
     * @param Request $request
     * @return object
     */
    public function SuccessMessage(Request $request):object
    {
        $strInfo = '';
        foreach($request->query as $param){
            $strInfo = $strInfo.': '.$param;
        }
        return new Response('<html><body><p>Result Transaction!!!</p><p>'.$strInfo.'</p></body></html>');
    }        
}
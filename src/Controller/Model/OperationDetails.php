<?php

namespace App\Controller\Model;

class OperationDetails {
    /**
     * Undocumented variable
     *
     * @var string
     */
    private $name;
    /**
     * Undocumented variable
     *
     * @var string
     */
    private $email;
    /**
     * Undocumented variable
     *
     * @var string
     */
    private $phoneNumber;
    private $token;
    private $amount;
    private $currency;
    private $description;


    public function __construct(string $name = '', string $email='', string $phoneNumber='',string $token = '',
        float $amount = 0, string $currency = '', string $description = '')
	{
        $this->name = $name;
        $this->email = $email;        
        $this->phoneNumber = $phoneNumber;
        $this->token = $token;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->description = $description;
    }

    public function getName():string {
        return $this->name;
    }

    public function setName(string $name) {
        $this->name;
    }
    
    public function getEmail():string {
        return $this->email;
    }

    public function setEmail(string $email) {
        $this->email;
    }

    public function getPhoneNumber():string {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber) {
        $this->phoneNumber;
    }

    public function getToken():string{
        return $this->token;
    }

    public function setToken(string $token){
        $this->token;
    }

    public function getAmount():int{
        return $this->amount;
    }

    public function setAmount(float $amount){
        $this->amount;
    }

    public function getCurrensy():string{
        return $this->currency;
    }

    public function setCurrency(string $currency){
        $this->currency;
    }

    public function getDescription():string{
        return $this->description;
    }

    public function setDescription(string $description){
        $this->description;
    }    
}
<?php

namespace App\Repository\StripeRepo;

interface BaseInterface{
    public function JobBase();

    //public function IsValidation();
    public function EnterStripe(object $stripeController);

    public function EnterBase(bool $flag);
}
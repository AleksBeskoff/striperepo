<?php

namespace App\Repository\StripeRepo;

use App\Entity\TransactTable;
use App\Entity\UsersTable;
use App\Repository\StripeRepo\BaseInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;

class StripeBase implements BaseInterface 
{
    private $doctrine;
    private $conn;
    private $transactTable;
    private $usersTable;
    private $validator;
    private $operationDetails;

    public function __construct(object $doctrine, ValidatorInterface $validator,object $operationDetails)
	{
        $this->doctrine = $doctrine;
        $this->conn = $doctrine->getConnection();
        $this->transactTable = new TransactTable;
        $this->usersTable = new UsersTable;
        $this->validator = $validator;
        $this->operationDetails = $operationDetails;
	}
    

    public function JobBase():array
    {
        $this->conn->beginTransaction();
             
        try {
            $this->usersTable->setName($this->operationDetails->getName());
            $this->usersTable->setEmail($this->operationDetails->getEmail());
            $this->usersTable->setPhoneNumber($this->operationDetails->getPhoneNumber());
            $this->doctrine->getManager()->persist($this->usersTable);

            $messageValid = $this->IsValidation();
            if($messageValid!==''){
                throw new Exception($messageValid,1078);
            }            
            
        } catch(Exception $e) {
            //throw $e; //\error_log($e->getCode().'---'.$e->getMessage());
            return array('errorCode'=>$e->getCode(),'errorMessage'=>$e->getMessage());
        }
        $this->doctrine->getManager()->flush();
        return array();    
    }

    /**
     * Валидация данных (электронная почта и номер телефона) владельца карты перед совершением операции
     * Возвращаем строку содержащую сообщение об ошибках валидации, либо пустое значение 
     *
     * @return string
     */
    private function IsValidation():string
    {
        $errors = $this->validator->validate($this->usersTable);
        $errorMessages = '';        
        if(count($errors)>0){            
            foreach($errors as $error){
                $errorMessages =  $errorMessages.$error->getMessage().'; ';
            }
            return $errorMessages;
        }
        return $errorMessages;    
    }

    public function EnterBase($flag = true):array
    {
        try{
            if($flag==true){
                $this->conn->commit();
            }else{
                $this->conn->rollBack();
            }            
        } catch(Exception $e){            
            return array('status'=>false,'errorCode'=>$e->getCode(),'errorMessage'=>$e->getMessage());   
        }
        return array();
    }

    /**
     * Передаем запрос платежному сервису через объект stripecontroller на проведение операции, в случае отсутствия ошибок вносим
     * данные в базу данных.
     * В возвращаемом массиве содержатся данные по операции, либо данные об ошибке
     * 
     * @param object $stripeController
     * @return array
     */
    public function EnterStripe(object $stripeController):array
    {
        $arrDataStripe =  $stripeController->showData3(
            $this->operationDetails->getToken(),
            $this->operationDetails->getAmount(),
            $this->operationDetails->getCurrensy(),
            $this->operationDetails->getDescription()
            );
        
        if($arrDataStripe['status']=='succeeded') {
            $this->transactTable->setToken($this->operationDetails->getToken());
            $this->transactTable->setAmount($this->operationDetails->getAmount());
            $this->transactTable->setCurrency($this->operationDetails->getCurrensy());
            $this->transactTable->setDesctiption($this->operationDetails->getDescription());
            $this->transactTable->setIdTransaction($arrDataStripe['id']);
            $this->transactTable->setIdUser($this->usersTable->getId());
            $this->doctrine->getManager()->persist($this->transactTable);
            $this->doctrine->getManager()->flush();
        }
        return $arrDataStripe;   
    }
}